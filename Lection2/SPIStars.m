//
//  SPIStars.m
//  Lection2
//
//  Created by Admin on 08.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import "SPIStars.h"

@implementation SPIStars


- (instancetype)initWithName:(NSString *)name {
    self = [super initWithType:SPISpaceObjectTypeStars name:name];
    if (self) {
        
    }
    return self;
}

- (NSNumber*) whatWeight{
    return _weight;
}

+ (NSString *)typeStringForType:(StarType)type1{
    switch (type1) {
        case StarTypeGiant:
            return @"Giant Star";
            
        case StarTypeDwarf:
            return @"Dwarf Star";
            
        default:
            return @"Unknown";
    }}

- (instancetype)initWithType:(StarType)type1 name1:(NSString *)name1 {
    
    self = [super initWithType:SPISpaceObjectTypeStars name:name1];
    
    if (self) {
        _type1 = type1;
        _name1 = name1;
    }
    return self;
}


- (NSString *)description {
    return [NSString stringWithFormat:@"\n Star: %@\n Weight: %@\n Type: %@ \nDestroing: %@",
            self.name1, self.weight, [SPIStars typeStringForType:self.type1], self.dest ? @"YES" : @"NO"];
}


@end
