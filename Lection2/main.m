//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>

#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"

#import "SPIPlanet.h"
#import "SPIAsteroidField.h"
#import "SPIPlayerSpaceship.h"
#import "SPIStars.h"

int main(int argc, const char * argv[]) {
    
    SPIStarSystem *RedStarSystem = [[SPIStarSystem alloc] initWithName:@"Red" age:@(5000000)];
    SPIStarSystem *BlackStarSystem = [[SPIStarSystem alloc] initWithName:@"Black" age:@(7500000)];
    SPIStarSystem *GreenStarSystem = [[SPIStarSystem alloc] initWithName:@"Green" age:@(10000000)];
    SPIStarSystem *BlueStarSystem = [[SPIStarSystem alloc] initWithName:@"Blue" age:@(12000000)];
    
    
    NSArray *name_system = @[RedStarSystem.name, BlackStarSystem.name, GreenStarSystem.name, BlueStarSystem.name];
    NSArray *systems = @[RedStarSystem, BlackStarSystem, GreenStarSystem, BlueStarSystem];
    
    
  
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 325000000;
    vulkanPlanet.dest = NO;
    
    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    hotaAsteroidField.dest = NO;
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    gallifreiPlanet.dest = NO;
    
 
    
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    nabooPlanet.dest =  YES;
    
  
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = YES;
    plutoPlanet.dest = YES;
    
    SPIPlanet *UpiterPlanet = [[SPIPlanet alloc] initWithName:@"Upiter"];
    UpiterPlanet.atmosphere = YES;
    UpiterPlanet.peoplesCount = 625000000;
    UpiterPlanet.dest = YES;
    
    SPIPlanet *ArtemPlanet = [[SPIPlanet alloc] initWithName:@"Artem"];
    ArtemPlanet.atmosphere = YES;
    ArtemPlanet.peoplesCount = 625000000;
    ArtemPlanet.dest = YES;
    
    
    SPIStars *oneStar = [[SPIStars alloc] initWithType: StarTypeGiant name1:@"Maxima" ];
    oneStar.weight = @(700000000);
    oneStar.dest = YES;
    
    SPIStars *secondStar = [[SPIStars alloc] initWithType:StarTypeDwarf name1: @"Proxima"];
    secondStar.weight = @(24500000);
    secondStar.dest = YES;
    
    SPIAsteroidField *firstAsteroidField = [[SPIAsteroidField alloc] initWithName:@"AsteroidBig"];
    firstAsteroidField.density = 40000000;
    firstAsteroidField.dest = YES;
    
    SPIAsteroidField *secondAsteroidField = [[SPIAsteroidField alloc] initWithName:@"AsteroidPig"];
    secondAsteroidField.density = 300000;
    secondAsteroidField.dest = YES;
    
  

    RedStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[vulkanPlanet, hotaAsteroidField, gallifreiPlanet, nabooPlanet]];
    BlackStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[UpiterPlanet, secondStar, secondAsteroidField]];
    GreenStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[firstAsteroidField, oneStar, plutoPlanet]];
    BlueStarSystem.spaceObjects = [[NSMutableArray alloc] initWithArray:@[ArtemPlanet]];
    
    
    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"OdBEDA"];
    [spaceship loadStarSystem: systems systems_name: name_system];
    
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:RedStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:BlackStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:GreenStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:BlueStarSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    
    
    BOOL play = YES;
    
    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}

