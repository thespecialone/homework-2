//
//  SPIStars.h
//  Lection2
//
//  Created by Admin on 08.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIStars : SPISpaceObject

@property (nonatomic, assign, getter = whatWeight) NSNumber* weight;

typedef NS_ENUM(NSInteger, StarType) {
    StarTypeGiant,
    StarTypeDwarf,
    StarTypeDefault
};

@property (nonatomic, assign) StarType type1;
@property (nonatomic, strong, readonly) NSString *name1;

- (instancetype)initWithType:(StarType)type1 name1:(NSString *)name1;



@end
